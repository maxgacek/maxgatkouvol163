﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseBallDrag : MonoBehaviour {

    [HideInInspector] public Rigidbody2D rb;

    public bool isDragged = false;
    public int distance;
    public bool giveForce = false;

    MouseBallDrag[] allBalls;

    public Vector2 myVelocity;

    private void Start() {
        allBalls = FindObjectsOfType<MouseBallDrag>();

        rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
    }

    private void OnMouseDrag() {
        rb.MovePosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        isDragged = true;
        rb.isKinematic = false;
        giveForce = false;
    }
    private void OnMouseUp() {
        isDragged = false;
        giveForce = true;
    }

    private void OnCollisionEnter2D(Collision2D collision) {

        if (giveForce && isDragged == false) setForce();

    }

    private void Update() {

        if (rb.velocity != Vector2.zero) myVelocity = rb.velocity;
    }

    void setForce() {
        giveForce = false;

        MouseBallDrag targetBall = null;

        foreach (MouseBallDrag ball in allBalls) {

            resetBall(ball.rb);
            ball.giveForce = false;

            if (ball.distance == distance && ball != this) {
                targetBall = ball;
                Debug.Log(ball.gameObject);
            }
        }

        targetBall.giveForce = true;
        targetBall.rb.isKinematic = false;
        targetBall.rb.AddForce(myVelocity, ForceMode2D.Impulse);
    }


    void resetBall(Rigidbody2D rb) {
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0f;
    }

}
