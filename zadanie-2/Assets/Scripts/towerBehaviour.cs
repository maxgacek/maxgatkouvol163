﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towerBehaviour : MonoBehaviour {

    public GameObject bulletPrefab;
    public Transform spawnPos;

    SpriteRenderer sprite;

    public bool wait = true;

    private void Start() {
        sprite = GetComponent<SpriteRenderer>();

        if (wait) Invoke("startAction", 6f);
        else StartCoroutine(towerAction());
    }

    public void startAction() {
        StartCoroutine(towerAction());
    }

    IEnumerator towerAction() {
        sprite.color = Color.red;

        for (int i = 0; i < 12; i++) {

            transform.Rotate(Vector3.forward, Random.Range(15, 45));
            yield return new WaitForSeconds(.5f);
            shootBullet();
        }

        sprite.color = Color.white;

        yield return 0;
    }

    private void shootBullet() {
        GameObject bullet = Instantiate(bulletPrefab, spawnPos.position, Quaternion.identity);

        bullet.transform.localRotation = transform.localRotation;
    }
}
