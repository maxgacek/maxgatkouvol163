﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class towersCount : MonoBehaviour {

    public Text towersNumberText;
    int towersNumber = 1;

    public bool createNewTowers = true;

    void Update () {

        towersNumber = FindObjectsOfType<towerBehaviour>().Length;

        towersNumberText.text = "Towers: " + towersNumber.ToString();

        if (towersNumber >= 100 && createNewTowers) {

            towerBehaviour[] towers = FindObjectsOfType<towerBehaviour>();

            foreach (towerBehaviour tower in towers) {
                tower.startAction();
            }

            createNewTowers = false;
        }

    }

}
