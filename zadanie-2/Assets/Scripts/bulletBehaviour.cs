﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletBehaviour : MonoBehaviour {

    Rigidbody2D rb;
    towersCount towersCount;

    public GameObject towerPrefab;

    int bulletSpeed = 4;
    int travelDistance = 1;
    Vector2 travelTarget = Vector2.zero;

    private void Start() {
        rb = GetComponent<Rigidbody2D>();
        towersCount = FindObjectOfType<towersCount>();

        travelDistance = Random.Range(1, 4);

        travelTarget = transform.position + transform.up * travelDistance;
    }

    private void Update() {
        if (Vector2.Distance(transform.position, travelTarget) > 0.1)
            transform.position += transform.up * bulletSpeed * Time.deltaTime;
        else
            onDestination();
    }

    void onDestination() {

        if (towersCount.createNewTowers) {
            GameObject tower = Instantiate(towerPrefab, transform.position, Quaternion.identity);
            tower.transform.rotation = Quaternion.Euler(Vector3.zero);
        }

        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        Debug.Log("Collision");

        if (collision.gameObject.tag == "Tower") {
            Destroy(collision.gameObject);
            Debug.Log("DESTroy tower");
            Destroy(gameObject);
        }
    }
}
